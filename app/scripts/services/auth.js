'use strict';

var app = angular.module('preApp');

app.factory('Session', ['$resource' , function($resource){
    return $resource('/api/session/');
 }]);


app.factory('User', [ '$resource', function ($resource) {
        return $resource('/api/users/:id', {
            id: '@id'
        }, { 
            update: {
                method: 'PUT'
            },
            get: {
                method: 'GET',
                params: {
                    id: 'me'
                }
            },
            delete: {
                method: 'DELETE',
                params: {}
            }
        });
    }]);


app.factory('Auth', ['$location', '$rootScope', 'Session', 'User', '$cookieStore', function Auth($location, $rootScope, Session, User, $cookieStore) {

        // Get currentUser from cookie
        $rootScope.currentUser = $cookieStore.get('user') || null;
        $cookieStore.remove('user');

        return {

            /**
             * Authenticate user
             *
             * @param  {Object}   user     - login info
             * @param  {Function} callback - optional
             * @return {Promise}
             */
            login: function (user, callback) {
                var cb = callback || angular.noop;

                return Session.save({
                    email: user.email,
                    password: user.password
                }, function (user) {
                    $rootScope.currentUser = user;
                    return cb();
                }, function (err) {
                    return cb(err);
                }).$promise;
            },

            /**
             * Unauthenticate user
             *
             * @param  {Function} callback - optional
             * @return {Promise}
             */
            logout: function (callback) {
                var cb = callback || angular.noop;

                return Session.delete(function () {
                        $rootScope.currentUser = null;
                        return cb();
                    },
                    function (err) {
                        console.log(err);
                        return cb(err);
                    }).$promise;
            },

            /**
             * Create a new user
             *
             * @param  {Object}   user     - user info
             * @param  {Function} callback - optional
             * @return {Promise}
             */
            createUser: function (user, callback) {
                var cb = callback || angular.noop;

                return User.save(user,
                    function (user) {
                        $rootScope.currentUser = user;
                        console.log(user);
                        return cb(user);
                    },
                    function (err) {
                        console.log(user);
                        return cb(err);
                    }).$promise;
            },
            /**
             * Create a new user
             *
             * @param  {Object}   user     - user info
             * @param  {Function} callback - optional
             * @return {Promise}
             */
            verifyUser: function (pass, callback) {
                var cb = callback || angular.noop;
                console.log(pass);

                var pass1 = {
                    "password": "vkm123"
                };
                pass1 = JSON.stringify(pass1);

                console.log(pass1);

                return User.update(pass1,
                    function (user) {
                        $rootScope.currentUser = user;
                        console.log(user);
                        return cb(user);
                    },
                    function (err) {
                        console.log(err);
                        return cb(err);
                    }).$promise;
            },

            /**
             * Change password
             *
             * @param  {String}   oldPassword
             * @param  {String}   newPassword
             * @param  {Function} callback    - optional
             * @return {Promise}
             */
            changePassword: function (oldPassword, newPassword, callback) {
                var cb = callback || angular.noop;

                return User.update({
                    oldPassword: oldPassword,
                    newPassword: newPassword
                }, function (user) {
                    return cb(user);
                }, function (err) {
                    return cb(err);
                }).$promise;
            },

            /**
             * Gets all available info on authenticated user
             *
             * @return {Object} user
             */
            currentUser: function (callback) {
                var cb = callback || angular.noop;
                Session.get(function(user) {
                    $rootScope.currentUser = user;
                    cb(user);
                });
            },
            /*
             * Update User*/
            updateUser: function (data, callback) {
                var cb = callback || angular.noop;

                return User.update({
                    data: data
                }, function (user) {
                    return cb(user);
                }, function (err) {
                    return cb(err);
                }).$promise;
            },
            createRole: function (role, callback) {
                var cb = callback || angular.noop;

                return Role.save(role,
                    function (role) {
                        return cb(role);
                    },
                    function (err) {
                        return cb(err);
                    }).$promise;
            },
            updateRole: function (data, callback) {
                var cb = callback || angular.noop;

                return Role.update(data,
                    function (role) {
                        return cb(role);
                    }, function (err) {
                        return cb(err);
                    }).$promise;
            },
            deleteRole: function (role_id, callback) {
                var cb = callback || angular.noop;

                return Role.delete({
                    data: role_id
                }, function (role) {
                    return cb(role);
                }, function (err) {
                    return cb(err);
                }).$promise;
            },

            /**
             * Simple check to see if a user is logged in
             *
             * @return {Boolean}
             */
            isLoggedIn: function () {
                var user = $rootScope.currentUser;
                return !!user;
            }
        };
    }]);
