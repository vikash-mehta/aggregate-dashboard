
var myApp = angular.module('myApp', ['angle','uiGmapgoogle-maps', 'preApp']);

myApp.config(['uiGmapGoogleMapApiProvider', function (GoogleMapApi) {
    GoogleMapApi.configure({
//    key: 'your api key',
        v: '3.17',
        libraries: 'weather,geometry,visualization'
    });
}]);
myApp.run(["$log", function ($log) {
    $log.log('I\'m a line from custom.js');

}]);

myApp.config(['$stateProvider', '$locationProvider', '$urlRouterProvider', 'RouteHelpersProvider', '$httpProvider',
    function ($stateProvider, $locationProvider, $urlRouterProvider, helper, $httpProvider) {
        'use strict';

        $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

        // Set the following to true to enable the HTML5 Mode
        // You may have to set <base> tag in index and a routing configuration in your server
       $locationProvider.html5Mode({
          enabled: true,
          requireBase: false
        });

    
        //App routes
    $stateProvider
        .state('dashboard', {
            url: '/',
            title: 'Dashboard',
            templateUrl: 'partials/dashboard.html',
            resolve: helper.resolveFor('datatables', 'datatables-pugins', 'ngDialog', 'filestyle')

        })
        .state('chart', {
            url: '/chart',
            title: 'Chart',
            templateUrl: 'partials/chart.html',
            resolve: helper.resolveFor('datatables', 'datatables-pugins', 'ngDialog', 'filestyle')

        });


        // Default route
        $urlRouterProvider.otherwise('/');


        $httpProvider.defaults.transformRequest = function(data){
            if (data === undefined) {
                return data;
            }
            return $.param(data);
        }


    }]);
