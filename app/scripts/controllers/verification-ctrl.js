/**=========================================================
 * Module: access-register.js
 * Demo for register account api
 =========================================================*/
var app = angular.module('preApp');

app.controller('VerifyFormController', ['$scope', '$http', '$state', 'Auth', function ($scope, $http, $state, Auth) {
    //var local_data =!{JSON.stringify(name)};


    $scope.account = {};
    $scope.authMsg = '';

    $scope.verify = function () {
        $scope.authMsg = '';

        $http.post('/signupPassword', {
            password: $scope.account.password
        }).success(function (res) {
            $state.go('login');
            console.log(res);
        }).error(function (err) {
            console.log('Failure loading data', err);
            $state.go('login');
        });
    }

}]);
