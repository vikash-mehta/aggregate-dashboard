
angular.module('preApp').controller('LoginController', ['$scope', 'Auth', '$window', '$http',function ($scope, Auth, $window, $http) {

    $scope.account = {};
    $scope.authMsg = '';


    $scope.loginAdmin = function (form) {

            Auth.login({
                email: $scope.account.email,
                password: $scope.account.password
            }).then(function () {
                    // Logged in, redirect to home
                    $window.location.href = '/';
                })
                .catch(function (error) {
                    error = error.data;
                    $scope.serversideErrors.other = error.message;
                });
    };

    $scope.logout = function () {
        Auth.logout().then(function (e) {
            console.log('logout');
            $window.location.href = '/';
        });;
    }
}]);

