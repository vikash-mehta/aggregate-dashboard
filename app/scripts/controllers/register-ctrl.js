/**=========================================================
 * Module: access-register.js
 * Demo for register account api
 =========================================================*/
var app = angular.module('preApp');

app.controller('RegisterFormController', ['$scope', '$http', '$state', 'Auth', function ($scope, $http, $state, Auth) {

    $scope.account = {};
    $scope.authMsg = '';

    $scope.register = function () {
        $scope.authMsg = '';

        Auth.createUser({
            email: $scope.account.email, 
            name: $scope.account.name, 
            role: 1
        })
        .then(function (response) {
            if (response.email) {
                $scope.authMsg = response;
                $state.go('thanks');
            } else {
                $state.go('app.dashboard');
            }
        })
        .catch(function (err) {
            $scope.authMsg = 'Server Request Error' + err;
        });
    };

}]);
