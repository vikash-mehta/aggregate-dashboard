/**
 * Created by sanjay on 3/28/15.
 */

angular.module('myApp').controller('chartController', function ($scope, $http,$timeout) {
    $scope.data={};
    $scope.endDate = new Date();
    $scope.endDate = new Date($scope.endDate.setDate($scope.endDate.getDate() - 1));
    $scope.startDate = new Date();
    $scope.startDate =new Date($scope.startDate.setDate($scope.endDate.getDate() - 8));
    $scope.showError = "";

    $scope.drawChart = function() {
        $scope.chart = {
            chart: {
                type: 'area'
            },
            title: {
                text: 'Transaction on Daily Basis'
            },
            subtitle: {
                text: 'Source: Click Labs'
            },
            xAxis: {
                categories: [],
                tickmarkPlacement: 'on',
                title: {
                    enabled: false
                }
            },
            yAxis: {
                title: {
                    text: 'Number'
                },
                labels: {
                    formatter: function () {
                        return this.value;
                    }
                }
            },
            tooltip: {
                shared: true,
                valueSuffix: ''
            },
            plotOptions: {
                area: {
                    stacking: 'normal',
                    lineColor: '#666666',
                    lineWidth: 1,
                    marker: {
                        lineWidth: 1,
                        lineColor: '#666666'
                    }
                }
            },
            series: []
        };

        if(!$scope.startDate || !$scope.endDate){
            $scope.showError = "Something is wrong with Input";
            return;
        }

        if($scope.startDate > $scope.endDate){
            $scope.showError = "Something is wrong with Input";
            return;
        }

        $scope.showError = "";

        $http.post('/chartData', {
            startDate: $scope.startDate,
            endDate: $scope.endDate
        }, {cache: true})
            .success(function (data) {
                var newDateArray = [];
                var dateArray = [];

                data[0].result.forEach(function(result){
                    dateArray.push(result.date);
                });

                dateArray.forEach(function (e) {
                    e = new Date(e);
                    newDateArray.push(e.getDate() + '/' + (e.getMonth() + 1) + '/' + e.getFullYear());
                })

                $scope.chart.xAxis.categories = newDateArray;


                data.forEach(function(e){
                    var yAxis = [];
                    e.result.forEach(function(result){
                        yAxis.push(result.count);
                    })
                    $scope.chart.series.push({
                        name: e.project,
                        data: yAxis
                    })
                });

                $('#container').highcharts($scope.chart);

            })
            .error(function (err) {
                console.log('Failure loading data');
            });
    }

    $scope.drawChart();
});
