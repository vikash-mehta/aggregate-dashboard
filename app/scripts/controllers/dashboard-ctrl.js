/**
 * Created by sanjay on 3/28/15.
 */

angular.module('myApp').controller('DashboardController', function ($scope, $http,$timeout) {
    $scope.data=[];

    $http.get('/tableData')
        .success(function (data) {
            console.log(data);
            $scope.data = data;

            $timeout(function () {
                if (!$.fn.dataTable) return;
                dtInstance = $('#datatable').dataTable({
                    'paging': true,
                    'ordering': true,
                    'info': true,
                    oLanguage: {
                        sSearch: 'Search',
                        sLengthMenu: '_MENU_ Show ',
                        info: 'Showing page _PAGE_ of _PAGES_',
                        zeroRecords: 'Nothing found - sorry',
                        infoEmpty: 'No records available',
                        infoFiltered: '(filtered from _MAX_ total records)'
                    },
                    "pageLength": 50
                });
            });
        })
        .error(function (data, status, headers, config) {
            console.log('Failure loading data');
        });
});
