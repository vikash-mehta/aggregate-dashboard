
var preApp = angular.module('preApp', ['angle','uiGmapgoogle-maps']);

preApp.config(['uiGmapGoogleMapApiProvider', function (GoogleMapApi) {
    GoogleMapApi.configure({
//    key: 'your api key',
        v: '3.17',
        libraries: 'weather,geometry,visualization'
    });
}]);

preApp.run(["$log", function ($log) {
    $log.log('I\'m a line from custom.js');

}]);

preApp.config(['$stateProvider', '$locationProvider', '$urlRouterProvider', 'RouteHelpersProvider', '$httpProvider',
    function ($stateProvider, $locationProvider, $urlRouterProvider, helper, $httpProvider) {
        'use strict';

        console.log('I am pre app');

        $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

        // Set the following to true to enable the HTML5 Mode
       $locationProvider.html5Mode({
          enabled: true,
          requireBase: false
        });

        // Default route
        $urlRouterProvider.otherwise('/login');

        // Application Routes
        $stateProvider
            .state('login', {
                url: '/login',
                title: "Login",
                templateUrl: 'partials/no-auth/login.html',
                resolve: helper.resolveFor('modernizr', 'icons', 'parsley')
            })
            .state('register', {
                url: '/register',
                title: "Register",
                templateUrl: 'partials/no-auth/register.html',
                resolve: helper.resolveFor('modernizr', 'icons', 'parsley')
            })
            .state('verify', {
                url: '/verify/:token',
                title: "Verify",
                templateUrlUrl: 'partials/no-auth/verify.html',
                resolve: helper.resolveFor('modernizr', 'icons', 'parsley')
            })
            .state('recover', {
                url: '/recover',
                title: "Recover",
                templateUrl: 'partials/no-auth/recover.html',
                resolve: helper.resolveFor('modernizr', 'icons', 'parsley')
            })
            .state('terms', {
                url: '/terms',
                title: "Terms & Conditions",
                templateUrl: 'partials/no-auth/terms.html',
                resolve: helper.resolveFor('modernizr', 'icons', 'parsley')
            })
            .state('thanks', {
                url: '/thanks',
                title: "Thank U Note",
                templateUrl: 'partials/no-auth/thanks.html',
                resolve: helper.resolveFor('modernizr', 'icons', 'parsley')
            })
            .state('404', {
                url: '/404',
                title: "Not Found",
                templateUrl: 'partials/no-auth/404.html',
                resolve: helper.resolveFor('modernizr', 'icons', 'parsley')
            })

        $httpProvider.defaults.transformRequest = function(data){
            if (data === undefined) {
                return data;
            }
            return $.param(data);
        }


    }]);
