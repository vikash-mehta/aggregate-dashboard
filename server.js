'use strict';


// Module dependencies.
var express = require('express');

var app = express();
var router = express.Router();

var cfg = require('./config').Config;


// Express Configuration
require('./lib/config/express')(app);

// Routing
require('./lib/route')(app);


// Passport Configuration
require('./lib/config/passport')();

// Controllers
var index = require('./lib/controllers'),
    models = require('./lib/models');

// Start server
var port = process.env.PORT || 9000;

app.listen(port, function () {
    console.log('Express server listening on port %d in %s mode', port, app.get('env'));
});

