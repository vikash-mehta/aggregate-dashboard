'use strict';
var cfg = require('../../config').Config;

var fs        = require('fs'),
    path      = require('path'),
    Sequelize = require('sequelize'),
    sequelize = new Sequelize(cfg.users_db_name, cfg.users_db_user, cfg.users_db_password, {
        dialect: "postgres",
        host: cfg.users_db_host,
        port: cfg.users_db_port
      });
    var db        = {};

sequelize
  .authenticate()
  .then(function(err) {
    if (!!err) {
      console.log('Unable to connect to the database:', err)
    } else {
      console.log('Connection has been established successfully.')
    }
  })


fs.readdirSync(__dirname)
  .filter(function(file) {
    return (file.indexOf(".") !== 0) && (file !== "index.js");
  })
  .forEach(function(file) {
    var model = sequelize["import"](path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach(function(modelName) {
  if ("associate" in db[modelName]) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
//db.Sequelize = Sequelize;

module.exports = db;