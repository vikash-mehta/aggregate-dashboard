'use strict';
var Sequelize = require('sequelize');
var crypto = require('crypto');

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Session', {
    data: DataTypes.STRING,
    sid: DataTypes.STRING
  }, {
    tableName: 'sessions',
    updatedAt: 'updatedat',
    createdAt: 'createdat'
  });
};
