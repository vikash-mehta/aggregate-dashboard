'use strict';
var Sequelize = require('sequelize');
var crypto = require('crypto');

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('User', {
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    email: {type:DataTypes.STRING,unique: true},
    password: DataTypes.STRING,
    confirmation_token: DataTypes.STRING,
    activated: DataTypes.BOOLEAN,
    salt: DataTypes.STRING,
    sid: DataTypes.STRING
  }, {
    schema : 'users',
    tableName: 'users',
    updatedAt: 'updated_at',
    createdAt: 'created_at',
    setterMethods: {
      password: function(pass) {
        this.salt = this.makeSalt();
        var hashedPassword = this.encryptPassword(pass);
        this.setDataValue('password', hashedPassword);
      },
       userActivation: function(data) {
          this.setDataValue('confirmation_token', data.confirmation_token);
          this.setDataValue('activated', data.activated);
      },
      name: function(name) {
        var parts = name.split(' ');
        if(parts.length>0) {
          this.setDataValue('first_name', parts[0]);
        }
        if(parts.length>1) {
          this.setDataValue('last_name', parts[1]);
        }
      }
    },
    getterMethods: {
      userInfo: function() {
        return {
          'name': this.getDataValue('first_name'),
          'email': this.getDataValue('email'),
          'role_id': this.getDataValue('role_id'),
          'user_id': this.getDataValue('id')

        };
      },
      userProfile: function() {
        return {
          'name': this.getDataValue('first_name') +(this.getDataValue('last_name')===null ? "":(" " + this.getDataValue("last_name") )),
          'email': this.getDataValue('email')
        };
      }
    },
    classMethods: {
    },
    instanceMethods: {
      /**
       * Make salt
       *
       * @return {String}
       * @api public
       */
      makeSalt: function() {
        return crypto.randomBytes(16).toString('base64');
      },

      authenticate: function(plainText) {
        console.log("Matching passwords");
        console.log(this.encryptPassword(plainText));
        console.log(this.getDataValue('password'));
        console.log(this.password);
        return this.encryptPassword(plainText) === this.getDataValue('password');

      },

      /**
       * Encrypt password
       *
       * @param {String} password
       * @return {String}
       * @api public
       */
      encryptPassword: function(password) {
        if (!password || !this.salt) {
            return '';
        }
        var salt = new Buffer(this.salt, 'base64');
        return crypto.pbkdf2Sync(password, salt, 10000, 64).toString('base64');
      }
    }
  });
};
