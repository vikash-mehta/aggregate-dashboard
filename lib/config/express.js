'use strict';

var express = require('express'),
    cookieParser = require('cookie-parser'),
    session = require('express-session'),
    bodyParser = require('body-parser'),
    errorHandler = require('errorhandler'),
    serveStatic = require('serve-static'),
    path = require('path'),
    passport = require("passport"),
    SequelizeStore = require('connect-session-sequelize')(session.Store),
    seq = require('../models');

var favicon = require('serve-favicon');
var logger = require('morgan');
var methodOverride = require('method-override');


module.exports = function(app) {

  var rootPath = path.normalize(__dirname + '/../..');

  if ('development' == app.get('env')) {

      app.use(function noCache(req, res, next) {
      if (req.url.indexOf('/scripts/') === 0) {
        res.header("Cache-Control", "no-cache, no-store, must-revalidate");
        res.header("Pragma", "no-cache");
        res.header("Expires", 0);
      }
      next();
    });

    express.static.mime.define({'Image': ['png']});
      app.use(serveStatic(rootPath + '/app'));
      app.use(errorHandler());
    app.set('views', rootPath + '/app/views');
  }

 if ('production' == app.get('env')) {
    app.use(favicon(path.join(rootPath, 'public', 'favicon.ico')));
     app.use(serveStatic(rootPath + '/app'))
    app.set('views', rootPath + '/app/views');
  }

  app.engine('html', require('ejs').renderFile);
  app.set('view engine', 'html');
  app.use(logger('dev'));
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(methodOverride());
  app.use(cookieParser());


  app.use(session(
      {
          resave: false,
          saveUninitialized: false,
          secret: 'uwotm8',
          store: new SequelizeStore({
              db: seq.sequelize,
              table: 'Session'
          })
      }
  ));
    app.use(passport.initialize());
    app.use(passport.session());

};
