'use strict';

var db = require('../models');
var User = db.User,
    passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy;


/**
 * Passport configuration
 */

module.exports = function() {
  
  passport.serializeUser(function(user, done) {
    done(null, user.email);
  });

  passport.deserializeUser(function(email, done) {
    User.findOne({where: {email: email}}).then(function(user) {
      done(null, user);
    }).error(function(err) {
      done(err, null);
    });
  });

  passport.use(new LocalStrategy({
      usernameField: 'email',
      passwordField: 'password' 
    },
    function(username, password, done) {
      User.findOne({where : { email: username }}).then(function (user) {
        if (!user) {
          return done(null, false, { message: 'Incorrect username.' });
        }
        if (!user.authenticate(password)) {
          return done(null, false, { message: 'Incorrect password.' });
        }
        return done(null, user);
      });
    }
  ));

};
