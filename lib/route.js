'use strict';


var services = require('./controllers/services'),
    index = require('./controllers'),
    login = require('./controllers/login'),
    session = require('./controllers/session'),
    services = require('./controllers/services'),
    user = require('./controllers/user');

var db = require('./models');
var User = db.User,
    Role = db.Role,
    Display = db.Display;

var middleware = require('./middleware');

function ensureUnauthenticated(req, res, next) {
    if (req.isAuthenticated()) {
        res.redirect('/');
    }
    else {
        return next();
    }
}

function ensureAuthenticated(req, res, next) {
    if(req.body.data && req.body.data.signingup){
        if(req.isAuthenticated()) {
            res.redirect('/');
        }
        else {
            return next();
        }
    }
    else if (req.isAuthenticated()) {
        return next();
    }
    else {
        res.redirect('/login');
    }
}

function authenticateDisplay(req, res, next) {
    if (req.isAuthenticated()) {
        Display.find({ where: { id: req.params.display_id } }).success(function (display) {
            if(display.user_id === req.user.id) {
                return next();
            }
            res.send(401);
        });
    }
    else {
        res.redirect('/login');
    }
}

module.exports = function (app) {
   
    /*
     *Blitz Authorization
     */
    app.get('/mu-6e692695-919edff1-0e35923e-8b5491e2', function(req, res){ res.send('42');});

    /*
     *  Do not require authentication
     */
    app.get('/partials/no-auth/*', ensureUnauthenticated, index.partials);
    app.get('/login', function(req, res,next){console.log('-----------------------------'); next();}, ensureUnauthenticated, login.index);
    app.get('/register', ensureUnauthenticated, login.index);
     //app.param('token', function (req, res, next, token) {
     //  console.log('CALLED ONLY ONCE');
     //  next();
     //})
//     app.get('/verify/:token', function(req, res, next){console.log('--------123-------------'); res.render('index');} , ensureUnauthenticated, user.verifyUser);
    //http://localhost:3000/verify?token=eyJ0eX
     app.get('/verify',  ensureUnauthenticated, user.verifyUser);

    //app.get('/verify/user/', function(req, res){
    //    console.log('------------------',req.params.token);
    //    console.log(app.get('views'));
    //    console.log(__dirname);
    //    res.render('index', {name : 'vkm'});
    //});
    //
    //app.get('/verify',  function(req, res) {
    //    res.render('index');
    //});

    app.get('/recover', ensureUnauthenticated, login.index);
    app.get('/terms', ensureUnauthenticated, login.index);
    app.get('/thanks', ensureUnauthenticated, login.index);
    app.get('/404', ensureUnauthenticated, login.index);

    app.post('/api/session', ensureUnauthenticated, session.login);
    app.delete('/api/session', ensureAuthenticated, session.logout);

    app.post('/api/users', ensureUnauthenticated, user.createUser);
    app.post('/signupPassword',ensureUnauthenticated, user.updateUser);

    app.get('/welcome', ensureUnauthenticated, login.index);
    app.get('/welcome/:token', ensureUnauthenticated, login.index);
    app.get('/forgot_password', ensureUnauthenticated, login.index);
    app.get('/iforgotpassword/:token', ensureUnauthenticated, login.index);


    app.get('/tableData', ensureAuthenticated, services.tableData);
    app.post('/chartData', ensureAuthenticated, services.chartData);

    app.get('/partials/*', ensureAuthenticated, index.partials);
    app.get('/*', ensureAuthenticated, middleware.setUserCookie, index.index);
};
