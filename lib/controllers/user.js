var cfg = require('../../config.js').Config;

var path = require('path'),
    templatesDir = path.resolve(__dirname, '..', 'email-templates'),
    emailTemplates = require('email-templates'),
    nodemailer = require('nodemailer');

var jwt = require('jwt-simple'),
    jwtSecret = "VKM",
    moment = require('moment');

var db = require('../models'),
    User = db.User;

var transport = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: 'vikashmehta1512@gmail.com',
        pass: 'parasvis1'
    }
});

    var express = require('express');

    var app = express();

/**
 * Create user
 */
exports.createUser = function (req, res) {

    var newUser = {
        first_name : req.body.name,
        email : req.body.email,
        role : 1
    }
    
    User.create(newUser).then(function(abc){
        /**
         *  Create a token and send a mail to user to verify mail.
         *
         */
        var payload = {email: req.body.email};
        var token = jwt.encode(payload, jwtSecret);
       User.update({confirmation_token: token, activated: false},
        { where: { email: req.body.email }}).then(function (user) {
            
                if(user[0]){
                    emailTemplates(templatesDir, function (err, template) {
                        if (err) {
                            console.log(err);
                        }
                        else {
                            var link;
                            if (cfg.app_port === "80") {
                                link = 'http://'+cfg.app_host+'/verify?token=' + token;
                            }
                            else {
                                link = 'http://'+cfg.app_host+':'+cfg.app_port+'/verify?token=' + token;
                            }
                            var locals = {
                                email: req.body.email,
                                name: req.body.name,
                                link: link
                            };

                            template('signup', locals, function (err, html, text) {
                                if (err) {
                                    console.log(err);
                                }
                                else {
                                    transport.sendMail({
                                        from: 'Support <'+cfg.app_mailer+'>',
                                        to: locals.email,
                                        subject: 'Please Verify Your Email',
                                        html: html,
                                        text: text
                                    }, function (err, responseStatus) {
                                        if (err) {
                                            console.log('This is the error', err);
                                            res.status(500).json(err);
                                        }
                                        else {
                                            res.status(200).json({email:req.body.email});
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
                
            
        }).catch(function (err) {
            return res.status(500).json(err);
        });
    }).catch(function(err){
        return res.status(500).json(err);
    });
};

exports.verifyUser =  function (req, res, next) {
    var token = req.query.token;
    email = jwt.decode(token, jwtSecret).email;

      User.find({where: {email: email}}).then(function(user){

        if(user.password) {
          res.status(401).json({
            status: "failure",
            code: 401,
            message: "Expired Token"
          });
        }else {
            res.setHeader("Access-Control-Allow-Origin", "*");
            return res.render('index', {
                     name: user.first_name + ((user.last_name === null)? "":(" " + user.last_name)),
                     email: user.email
                 });

        }
      });
};

exports.updateUser =  function (req, res, next) {
    var token = req.query.token;
    var referer = req.headers.referer;
    var token = referer.split('token=')[1];
    email = jwt.decode(token, jwtSecret).email;

      User.find({where: {email: email}}).then(function(user){

        if(user.password) {
          res.status(401).json({
            status: "failure",
            code: 401,
            message: "Expired Token"
          });
        }else {
            user.update({
                password: req.body.password
            }).then(function(user) {
                console.log(user);
            })

        }
      });
};
