'use strict';


var passport = require('passport');

/*
*  Send User info on request. Helps in cases like page refresh.
* */
exports.session = function (req, res) {
    res.json(req.user.user_info);
};

/**
 * Logout
 */
exports.logout = function (req, res) {
    req.logout();
    res.send(200);
};

/**
 * Login
 */
exports.login = function (req, res, next) {
  passport.authenticate('local', function(err, user, info) {

      if (err) {
        return next(err); 
    }
    if (!user) { 
        return res.redirect('/login'); 
    }
    req.logIn(user, function(err) {
        if (err) { return next(err); }
      res.json(req.user.userInfo);
    });
  })(req, res, next);

};
