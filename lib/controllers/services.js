var app_json = require('../../application.json');
var mysql = require('mysql'),
    async = require("async");


exports.tableData = function(req, res) {

    var apps = app_json.app;
    var responseData = [];

    function getData(app, callback) {

        console.log('connecting to ',
            ' host: ', app.host,
            ', user: ', app.user,
            ', password: ', app.password,
            ', database: ', app.database,
            ', port: ', app.port);


        var connection = mysql.createConnection(
            {
                host: app.host,
                user: app.user,
                password: app.password,
                database: app.database,
                multipleStatements: true,
                port: app.port,
                connectTimeout: 200000
            }
        );

        connection.connect(function (err) {
            if (err) {
                console.log('error when connecting to db:', err);
                return;
            }
            async.parallel({
                total_users : function(callback){
                    var sql = app.get_total_users;
                    connection.query(sql, function (err, result) {
                        if(err){
                            console.log('total_user',err);
                            callback(null, 0);
                        }
                        
                        callback(null, result[0]['count(*)']);
                    });
                },
                total_transaction : function(callback){
                    var sql = app.get_total_transaction;
                    connection.query(sql, function (err, result) {
                        if(err){
                            console.log('total_trans',err);
                            callback(null, 0);
                        }
                        
                        callback(null, result[0]['count(*)']);
                    });
                },
                today_transaction : function(callback){
                    var endDate = new Date();
                    var startDate = new Date(endDate.setDate(endDate.getDate() - 1));

                    startDate =startDate.getFullYear()  + '-' + (startDate.getMonth() + 1) + '-' + (startDate.getDate()-1);
                    endDate =endDate.getFullYear()  + '-' + (endDate.getMonth() + 1) + '-' + endDate.getDate();

                    var sql = app.get_today_transaction + "DATE(" + app.column_name_transaction + ") >= '" + startDate + "' and DATE(" + app.column_name_transaction + ") < '" + endDate + "'";

                    connection.query(sql, function (err, result) {
                        if(err){
                            console.log('today_trans',err);
                            callback(null, 0);
                        }
                        
                        callback(null, result[0]['count(*)']);
                    });
                },
                total_revenue : function(callback){
                    var sql = app.get_total_revenue;
                    connection.query(sql, function (err, result) {
                        if(err){
                            console.log('total_rev',err);
                            callback(null, 0);
                        }
                        
                        callback(null, result[0][app.column_name_revenue]);
                    });
                },
                today_revenue : function(callback){
                    var endDate = new Date();
                    var startDate = new Date(endDate.setDate(endDate.getDate() - 1));

                    startDate =startDate.getFullYear()  + '-' + (startDate.getMonth() + 1) + '-' + (startDate.getDate()-1);
                    endDate =endDate.getFullYear()  + '-' + (endDate.getMonth() + 1) + '-' + endDate.getDate();

                    var sql = app.get_today_revenue + "DATE(" + app.column_name_revenue + ") >= '" + startDate + "' and DATE(" + app.column_name_revenue + ") < '" + endDate + "'";
                    connection.query(sql, function (err, result) {
                        if(err){
                            console.log('today_rev',err);
                            callback(null, 0);
                        }
                        
                        callback(null, result[0]['revenue']);
                    });
                }
        }, function(err, result){
                var responseObj = {
                    logo: "",
                    project: "",
                    total_users: "",
                    total_transaction: "",
                    today_transaction: "",
                    total_revenue: "",
                    today_revenue: ""
                };


                responseObj['logo'] = app.logo;
                responseObj['project']= app.project;
                responseObj['total_users'] = result['total_users'] || 0;
                responseObj['total_transaction'] = result['total_transaction'] || 0;
                responseObj['today_transaction'] = result['today_transaction'] || 0;
                responseObj['total_revenue'] = result['total_revenue'] || 0;
                responseObj['today_revenue'] = result['today_revenue'] || 0;

                responseData.push(responseObj);

                connection.end();
                callback(null);
            });
        });

    }


    async.each(apps, getData, function () {
        res.send(responseData);
    });

};

exports.chartData = function(req, res) {

    var apps = app_json.app;

    var startDate = new Date(req.body.startDate);
    var endDate = new Date(req.body.endDate);

    var responseData = [];

    startDate =startDate.getFullYear()  + '-' + (startDate.getMonth() + 1) + '-' + startDate.getDate();
    endDate =endDate.getFullYear()  + '-' + (endDate.getMonth() + 1) + '-' + endDate.getDate();

    function getData(app, callback) {

        responseData[app.project] = responseData[app.project] || {};
        console.log('connecting to ',
            ' host: ', app.host,
            ', user: ', app.user,
            ', password: ', app.password,
            ', database: ', app.database,
            ', port: ', app.port);


        var connection = mysql.createConnection(
            {
                host: app.host,
                user: app.user,
                password: app.password,
                database: app.database,
                multipleStatements: true,
                port: app.port,
                connectTimeout: 200000
            }
        );

        connection.connect(function (err) {
            if (err) {
                console.log('error when connecting to db:', err);
                return;
            }
            var sql = app.get_range_transaction + "DATE(" + app.column_name_transaction + ") > '" + startDate + "' and DATE(" + app.column_name_transaction + ") < '" + endDate + "' group by DATE(" + app.column_name_transaction +")";
                connection.query(sql, function (err, result) {
                    if(err){
                        console.log('total_user',err);
                        callback(null);
                    }
                    var responseObj = {
                        logo: "",
                        project: "",
                        result: []
                    };

                    responseObj['logo'] = app.logo;
                    responseObj['project']= app.project;
                    responseObj['result'] = result;
                    responseData.push(responseObj);
                    connection.end();
                    callback();

                });
            })
    }


    async.each(apps, getData, function () {
        res.send(responseData);
    });

};
