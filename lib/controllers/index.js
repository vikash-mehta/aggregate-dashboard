var path = require('path');

exports.partials = function (req, res) {
    console.log('partials');
    var stripped = req.url.split('.')[0];
    var requestedView = path.join('./', stripped);

    res.render(requestedView, function (err, html) {
        if (err) {
            res.render('404');
        } else {
            res.send(html);
        }
    });
};

exports.index = function(req, res) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.render('user');
};

exports.home = function(req, res) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.render('dashboard');
};

exports.admin = function(req, res){
  res.render('partials/admin');
};