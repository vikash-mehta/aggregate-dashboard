var development = {
    users_db_host: 'localhost',
    users_db_port: 5432,
    users_db_name: 'session',
    users_db_user: 'vkm',
    users_db_password: 'vkm123',
    data_db_host: 'localhost',
    data_db_port: 5432,
    data_db_name: 'vkm',
    data_db_user: 'vkm',
    data_db_password: 'vkm123',
    app_host: 'localhost',
    app_port: '9000',
    amqp_url: 'amqp://guest:guest@localhost:5672//',
    app_mailer: 'vikashmehta1512@gmail.com',
    admin_creds: {
        email: 'vikashmehta1512@gmail.com',
        name: {
            first: 'Vikash',
            last: ''
        }
    },
    log_file: 'app.log',
    env: global.process.env.NODE_ENV || 'development'
};

var production = {
    users_db_host: 'localhost',
    users_db_port: 5432,
    users_db_name: 'session',
    users_db_user: 'vkm',
    users_db_password: 'vkm123',
    data_db_host: 'localhost',
    data_db_port: 5432,
    data_db_name: 'vkm',
    data_db_user: 'vkm',
    data_db_password: 'vkm123',
    app_host: 'localhost',
    app_port: '9000',
    amqp_url: 'amqp://guest:guest@localhost:5672//',
    app_mailer: 'vikashmehta1512@gmail.com',
    admin_creds: {
        email: 'vikashmehta1512@gmail.com',
        name: {
            first: 'Vikash',
            last: 'Mehta'
        }
    },
    log_file: 'app.log',
    env: global.process.env.NODE_ENV || 'production'
};

exports.Config = global.process.env.NODE_ENV === 'production' ? production : development;
